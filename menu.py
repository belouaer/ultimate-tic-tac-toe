import pygame
import pygame_gui
import tictactoe
import mainAI

# Initialize Pygame
pygame.init()

def load_image(image_path):
    return pygame.transform.scale(pygame.image.load(image_path), (SCREEN_WIDTH, SCREEN_HEIGHT))

def create_button(position, text, manager):
    button_width, button_height = 270, 90
    return pygame_gui.elements.UIButton(relative_rect=pygame.Rect(position, (button_width, button_height)),
                                        text=text,
                                        manager=manager)

SCREEN_WIDTH, SCREEN_HEIGHT = pygame.display.Info().current_w, pygame.display.Info().current_h
WINDOW_SIZE = (SCREEN_WIDTH, SCREEN_HEIGHT)

screen = pygame.display.set_mode(WINDOW_SIZE, pygame.FULLSCREEN)
ui_manager = pygame_gui.UIManager(WINDOW_SIZE, 'theme.json')
tic = load_image("image/background.png")

def create_text_entry(position, manager):
    text_entry_width, text_entry_height = 280, 50
    return pygame_gui.elements.UITextEntryLine(relative_rect=pygame.Rect(position, (text_entry_width, text_entry_height)), 
                                               manager=manager)

def menu(a):
    pygame.display.set_caption("Main Menu")
    
    play_button = create_button((SCREEN_WIDTH / 2 - 125, SCREEN_HEIGHT / 3), 'Play Game', ui_manager)
    extras_button = create_button((SCREEN_WIDTH / 2 - 125, SCREEN_HEIGHT / 2), 'Extras', ui_manager)
    ai_button = create_button((SCREEN_WIDTH / 2 - 125, 3 * SCREEN_HEIGHT / 4.5), 'Play Against AI', ui_manager)
    
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            
            if event.type == pygame.USEREVENT and event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element == play_button:
                    ui_manager.clear_and_reset()
                    local_online(a)
                elif event.ui_element == extras_button:
                    ui_manager.clear_and_reset()
                    extras()
                elif event.ui_element == ai_button:
                    ui_manager.clear_and_reset()
                    mainAI.main()

            ui_manager.process_events(event)

        ui_manager.update(1.0 / 60.0)
        screen.blit(tic, (0, 0))
        ui_manager.draw_ui(screen)
        pygame.display.flip()
    
    pygame.quit()

def local_online(a):
    pygame.display.set_caption("Server/Client Menu")
    
    button_y = SCREEN_HEIGHT / 2 - 40
    server_button = create_button((SCREEN_WIDTH / 2 - 300, button_y), 'Local', ui_manager)
    client_button = create_button((SCREEN_WIDTH / 2 + 20, button_y), 'Online', ui_manager)
    return_button = create_button((50, SCREEN_HEIGHT - 100), 'Return Back', ui_manager)
    text_entry = None
    
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            
            if event.type == pygame.USEREVENT and event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element == server_button:
                    print("Server button clicked")
                    ui_manager.clear_and_reset()
                    tictactoe.main(a)
                elif event.ui_element == client_button:
                    ui_manager.clear_and_reset()
                    server_client(1)
                elif event.ui_element == return_button:
                    ui_manager.clear_and_reset()
                    menu(1)

            ui_manager.process_events(event)

        ui_manager.update(1.0 / 60.0)
        screen.blit(tic, (0, 0))
        ui_manager.draw_ui(screen)
        pygame.display.flip()
    
    pygame.quit()

def server_client(a):
    pygame.display.set_caption("Server/Client Menu")
    
    button_y = SCREEN_HEIGHT / 2 - 40
    server_button = create_button((SCREEN_WIDTH / 2 - 300, button_y), 'Client', ui_manager)
    client_button = create_button((SCREEN_WIDTH / 2 + 20, button_y), 'Server', ui_manager)
    return_button = create_button((50, SCREEN_HEIGHT - 100), 'Return Back', ui_manager)
    text_entry = None
    
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            
            if event.type == pygame.USEREVENT and event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element == server_button:
                    print("Server button clicked")
                    ui_manager.clear_and_reset()
                    tictactoe.main(a)
                elif event.ui_element == client_button:
                    print("Client button clicked")
                    if text_entry is None:
                        text_entry = create_text_entry((SCREEN_WIDTH / 2 - 140, SCREEN_HEIGHT / 2 + 100), ui_manager)
                    else:
                        entered_text = text_entry.get_text()
                        print(entered_text)
                elif event.ui_element == return_button:
                    ui_manager.clear_and_reset()
                    local_online(1)

            ui_manager.process_events(event)

        ui_manager.update(1.0 / 60.0)
        screen.blit(tic, (0, 0))
        ui_manager.draw_ui(screen)
        pygame.display.flip()
    
    pygame.quit()

def extras():
    pygame.display.set_caption("Extras")
    
    icon_button = create_button((SCREEN_WIDTH / 2 - 125, SCREEN_HEIGHT / 4), 'Choose Icon', ui_manager)
    music_button = create_button((SCREEN_WIDTH / 2 - 125, SCREEN_HEIGHT / 2), 'Choose Music', ui_manager)
    return_button = create_button((SCREEN_WIDTH / 2 - 125, 3 * SCREEN_HEIGHT / 4), 'Return Back', ui_manager)
    
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            
            if event.type == pygame.USEREVENT and event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element == icon_button:
                    ui_manager.clear_and_reset()
                    icon()
                elif event.ui_element == return_button:
                    ui_manager.clear_and_reset()
                    menu(1)
                elif event.ui_element == music_button:
                    ui_manager.clear_and_reset()
                    music_interface()

            ui_manager.process_events(event)

        ui_manager.update(1.0 / 60.0)
        screen.blit(tic, (0, 0))
        ui_manager.draw_ui(screen)
        pygame.display.flip()
    
    pygame.quit()

def icon():
    pygame.display.set_caption("Icon")
    
    Image1 = create_button((SCREEN_WIDTH / 2 - 125, SCREEN_HEIGHT / 4), 'Image 1', ui_manager)
    Image2 = create_button((SCREEN_WIDTH / 2 - 125, SCREEN_HEIGHT / 2), 'Image 2', ui_manager)
    Image3 = create_button((SCREEN_WIDTH / 2 - 125, 3 * SCREEN_HEIGHT / 4), 'Image 3', ui_manager)
    return_button = create_button((50, SCREEN_HEIGHT - 100), 'Return Back', ui_manager)
    
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            
            if event.type == pygame.USEREVENT and event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element == Image1:
                    ui_manager.clear_and_reset()
                    menu(1)
                elif event.ui_element == Image2:
                    ui_manager.clear_and_reset()
                    menu(2)
                elif event.ui_element == return_button:
                    ui_manager.clear_and_reset()
                    menu(1)

            ui_manager.process_events(event)

        ui_manager.update(1.0 / 60.0)
        screen.blit(tic, (0, 0))
        ui_manager.draw_ui(screen)
        pygame.display.flip()
    
    pygame.quit()

def music_interface():
    pygame.display.set_caption("Choose Music")
    
    music1_button = create_button((SCREEN_WIDTH / 2 - 125, SCREEN_HEIGHT / 4), 'Music 1', ui_manager)
    music2_button = create_button((SCREEN_WIDTH / 2 - 125, SCREEN_HEIGHT / 2), 'Music 2', ui_manager)
    music3_button = create_button((SCREEN_WIDTH / 2 - 125, 3 * SCREEN_HEIGHT / 4), 'Music 3', ui_manager)
    return_button = create_button((50, SCREEN_HEIGHT - 100), 'Return Back', ui_manager)
    
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            
            if event.type == pygame.USEREVENT and event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element == music1_button:
                    pass
                elif event.ui_element == music2_button:
                    pass
                elif event.ui_element == music3_button:
                    pass
                elif event.ui_element == return_button:
                    ui_manager.clear_and_reset()
                    menu(1)

            ui_manager.process_events(event)

        ui_manager.update(1.0 / 60.0)
        screen.blit(tic, (0, 0))
        ui_manager.draw_ui(screen)
        pygame.display.flip()
    
    pygame.quit()

menu(1)
